<?php

Route::group(['prefix' => config('settings.PREFIX'), 'middleware' => ['client_credentials','oauth.login', 'validate-permission', 'activity-tracking','caching']], function() 
{
    Route::group(['prefix' => 'users'], function()
    {
        /*
         * Checkout Controller Route
         */
        Route::resource(
            'masterpass/checkouts',
            '\App\Modules\MasterPass\Controllers\CheckoutController',
            [
                'only' => ['index']
            ]
        );
    });
});