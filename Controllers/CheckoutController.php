<?php

namespace App\Modules\MasterPass\Controllers;

use App\Modules\Core\Controllers\ActionController;
use App\Modules\MasterPass\Models\User\Checkout\Transaction as CheckoutTransaction;
use App\Modules\Core\Helpers\Pagination;
use App\Modules\Core\Helpers\Arr;

class CheckoutController extends ActionController
{
    /**
     * @SWG\Get(
     *   path="/api/users/masterpass/checkouts",
     *   tags={"masterpass"},
     *   summary="Retrieves Masterpass checkouts made by users",
     *   description="",
     *   operationId="api.users.masterpass.checkouts.index",
     *   produces={"application/xml", "application/json"},
     *   @SWG\Parameter(
     *     name="shipping_address",
     *     in="query",
     *     description="The customer's shipping address",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="billing_address",
     *     in="query",
     *     description="The user's billing addresss",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="merchant_name",
     *     in="query",
     *     description="The merchant name facilitating the checkout",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="with_pairing",
     *     in="query",
     *     description="A numeric value if a given checkout is paired",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="user_hash_id",
     *     in="query",
     *     description="The generated hash value of the corresponding user",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="date_created",
     *     in="query",
     *     description="The date of the checkout",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="card_number",
     *     in="query",
     *     description="The card number of checkout",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="first_name",
     *     in="query",
     *     description="The user's first name",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="last_name",
     *     in="query",
     *     description="The user's surname",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="sort_by",
     *     in="query",
     *     description="The fields to be sorted values are: `user_hash_id`, `first_name`, `last_name`,`card_number`, `merchant_checkout_id`, `merchant_name`, `billing_address`, `shipping_address`,`with_pairing`, `checkout_auth_request`, `checkout_auth_response`, `status`, `date_created`, `date_modified`",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="sort_type",
     *     in="query",
     *     description="Sort value in ascending (`asc`) or descending (`desc`) order",
     *     required=false,
     *     type="string"
     *   ),
     *  @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     description="Page to view",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="records_per_page",
     *     in="query",
     *     description="Records to display per page",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Response(
     *     response=400,
     *     description="Bad Request",
     *   ),
     *   @SWG\Response(
     *     response=401,
     *     description="Unauthorized",
     *   ),
     *   @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *   ),
     *   @SWG\Response(
     *     response=500,
     *     description="Internal Server Error",
     *   ),
     *   @SWG\Response(
     *      response=200,
     *      description="OK",
     *      @SWG\Schema(title="response", type="object", required={"status", "success", "message", "data"},
     *          @SWG\Property(property="status", type="integer", format="int64", description="Response code", default="200"),
     *          @SWG\Property(property="success", type="boolean", description="Is success", default="true"),
     *          @SWG\Property(property="message", type="string", description="Success message", default="Success"),
     *          @SWG\Property(title="Data", property="data", type="object", description="Data response",
     *              @SWG\Property(title="Pagination", property="pagination", type="object",
     *                  @SWG\Property(property="total_records", type="integer", format="int64", description="Total number of records", default="200"),
     *                      @SWG\Property(property="records_per_page", type="integer", format="int64", description="Number of records to display per page", default="10"),
     *                      @SWG\Property(property="total_pages", type="integer", format="int64", description="Total number of pages", default="20"),
     *                      @SWG\Property(property="page", type="integer", format="int64", description="Current page number", default="20"),
     *                      @SWG\Property(property="links", type="array",
                                @SWG\Items(
                                    @SWG\Property(
                                        property="self",
                                        type="string",
                                        description="Link to self",
                                        default="https://sgmc-sd-api-uat.mmvpay.com/api/api/users/masterpass/checkouts?records_per_page=10&page=1",
                                    ),
                                    @SWG\Property(
                                        property="first",
                                        type="string",
                                        description="Link to first page",
                                        default="https://sgmc-sd-api-uat.mmvpay.com/api/api/users/masterpass/checkouts?records_per_page=10&page=1",
                                    ),
                                    @SWG\Property(
                                        property="last",
                                        type="string",
                                        description="Link to last page",
                                        default="https://sgmc-sd-api-uat.mmvpay.com/api/api/users/masterpass/checkouts?records_per_page=10&page=9",
                                    ),
                                    @SWG\Property(
                                        property="next",
                                        type="string",
                                        description="Link to next page",
                                        default="https://sgmc-sd-api-uat.mmvpay.com/api/api/users/masterpass/checkouts?records_per_page=10&page=2",
                                    )
                                )
                            )
     *                  ),
     *                  @SWG\Property(title="Response", property="response", type="array", items=@SWG\Property(ref="#/definitions/MasterpassCheckout"))
     *              ),
     *          )
     *      )
     *   )
     * )
     *
     * @return json
     */

    /**
     * Retrieves Masterpass checkouts made by users
     *
     * @param CheckoutTransaction $masterpass_checkout
     * @return \App\Modules\Core\Traits\json
     */
    public function index(CheckoutTransaction $masterpass_checkout)
    {
        $validation_rules   =   [
            "shipping_address"  =>  "nullable|string",
            "records_per_page"  =>  "nullable|numeric",
            "billing_address"   =>  "nullable|string",
            "merchant_name"     =>  "nullable|string",
            "with_pairing"      =>  "nullable|numeric",
            "user_hash_id"      =>  "nullable|string",
            "date_created"      =>  "nullable|date:Y-m-d H:i:s",
            "card_number"       =>  "nullable|string",
            "first_name"        =>  "nullable|string",
            "sort_type"         =>  "nullable|in:".self::ASC.','.self::DESC,
            "last_name"         =>  "nullable|string",
            "sort_by"           =>  "nullable|in:".implode(',', array_keys($masterpass_checkout::NORMAL_FILTERS)),
            "page"              =>  "nullable|numeric",
        ];

        $validation_msg = [
            "shipping_address"  =>  trans('validation.checkouts.invalid_shipping_address'),
            "records_per_page"  =>  trans('validation.generic.invalid_records_per_page'),
            "billing_address"   =>  trans('validation.checkouts.invalid_billing_address'),
            "merchant_name"     =>  trans('validation.checkouts.invalid_merchant_name'),
            "with_pairing"      =>  trans('validation.checkouts.invalid_with_pairing'),
            "user_hash_id"      =>  trans('validation.checkouts.invalid_user_hash_id'),
            "date_created"      =>  trans('validation.generic.invalid_date'),
            "card_number"       =>  trans('validation.checkouts.invalid_card_number'),
            "first_name"        =>  trans('validation.checkouts.invalid_first_name'),
            "last_name"         =>  trans('validation.checkouts.invalid_last_name'),
            "sort_by"           =>  trans('validation.generic.invalid_sort_by'),
            "page"              =>  trans('validation.generic.invalid_offset'),
        ];

        $validated = $validated_params = $this->validateParameters($validation_rules, $validation_msg);

        if(! $validated)
        {
            return $this->respondValidationError($this->errors);
        }

        $validated_params['records_per_page'] = empty($validated_params['records_per_page']) ? $this->records_per_page : $validated_params['records_per_page'];

        $data = $masterpass_checkout->retrieve($validated_params);

        $result = [];

        if(! $data->isEmpty())
        {
            $result['pagination']   = Pagination::formatPagination($this->request, $data, $validated_params);
            $result['response']     = Arr::getPurifiedDataArray( $data->toArray()['data'] );
        }

        return $this->respondWithSuccess($result);
    }
}