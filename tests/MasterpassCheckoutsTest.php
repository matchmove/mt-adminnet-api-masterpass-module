<?php

namespace App\Modules\MasterPass\Tests;

use App\Modules\Core\tests\BaseTest;
use App\Modules\MasterPass\Models\User\Checkout\Transaction;

class MasterpassCheckoutsTest extends BaseTest
{
    /**
     * Positive test to get user masterpass checkouts
     */
    public function testGetMasterpassCheckouts()
    {
        if(Transaction::take(2)->get()->isEmpty())
        {
            $this->markTestSkipped('No data available');
        }

        $request_params = [
            'access_token' => $this->access_token,
        ];

        $response = $this->call('GET', 'api/users/masterpass/checkouts', $request_params);
        $response_array = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('status', $response_array);
        $this->assertArrayHasKey('success', $response_array);
        $this->assertArrayHasKey('data', $response_array);
        $this->assertArrayHasKey('message', $response_array);

        $this->assertEquals($response_array['status'], 200);
        $this->assertTrue($response_array['success']);
        $this->assertNotNull($response_array['data']);
    }

    /**
     * Positive test to get user masterpass checkouts with the given valid parameters
     */
    public function testGetMasterassCheckoutsWithValidParameters()
    {
        if(Transaction::take(2)->get()->isEmpty())
        {
            $this->markTestSkipped('No data available');
        }

        $checkout = Transaction::first();

        //-----------------------------------------
        // filter by user_hash_id
        //-----------------------------------------

        $request_params = [
            'access_token' => $this->access_token,
            'user_hash_id' => $checkout->user_hash_id
        ];

        $response = $this->call('GET', 'api/users/masterpass/checkouts', $request_params);
        $response_array = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('status', $response_array);
        $this->assertArrayHasKey('success', $response_array);
        $this->assertArrayHasKey('data', $response_array);
        $this->assertArrayHasKey('message', $response_array);

        $this->assertEquals($response_array['status'], 200);
        $this->assertTrue($response_array['success']);
        $this->assertNotNull($response_array['data']);

        //-----------------------------------------
        // filter by merchant_checkout_id
        //-----------------------------------------

        $request_params = [
            'access_token' => $this->access_token,
            'merchant_checkout_id' => $checkout->merchant_checkout_id
        ];

        $response = $this->call('GET', 'api/users/masterpass/checkouts', $request_params);
        $response_array = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('status', $response_array);
        $this->assertArrayHasKey('success', $response_array);
        $this->assertArrayHasKey('data', $response_array);
        $this->assertArrayHasKey('message', $response_array);

        $this->assertEquals($response_array['status'], 200);
        $this->assertTrue($response_array['success']);
        $this->assertNotNull($response_array['data']);

        //-----------------------------------------
        // filter by with_pairing
        //-----------------------------------------

        $request_params = [
            'access_token' => $this->access_token,
            'with_pairing' => $checkout->with_pairing
        ];

        $response = $this->call('GET', 'api/users/masterpass/checkouts', $request_params);
        $response_array = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('status', $response_array);
        $this->assertArrayHasKey('success', $response_array);
        $this->assertArrayHasKey('data', $response_array);
        $this->assertArrayHasKey('message', $response_array);

        $this->assertEquals($response_array['status'], 200);
        $this->assertTrue($response_array['success']);
        $this->assertNotNull($response_array['data']);

        //-----------------------------------------
    }

    /**
     * Negative test to retrieve masterpass checkouts with the given invalid parameters
     */
    public function testGetMasterpassCheckoutsWithInvalidParameters()
    {
        //-----------------------------------------
        // invalid with_pairing value
        //-----------------------------------------

        $request_parameters = [
            'access_token' => $this->access_token,
            'with_pairing' => 'not_a_number'
        ];

        $response = $this->call('GET','api/users/masterpass/checkouts', $request_parameters);
        $response_array = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('status', $response_array);
        $this->assertArrayHasKey('success', $response_array);
        $this->assertArrayHasKey('data', $response_array);
        $this->assertArrayHasKey('message', $response_array);

        $this->assertEquals($response_array['status'], 400);
        $this->assertFalse($response_array['success']);
        $this->assertNull($response_array['data']);

        //-----------------------------------------
        // invalid records_per_page value
        //-----------------------------------------

        $request_parameters = [
            'access_token' => $this->access_token,
            'records_per_page' => 'not_a_number'
        ];

        $response = $this->call('GET','api/users/masterpass/checkouts', $request_parameters);
        $response_array = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('status', $response_array);
        $this->assertArrayHasKey('success', $response_array);
        $this->assertArrayHasKey('data', $response_array);
        $this->assertArrayHasKey('message', $response_array);

        $this->assertEquals($response_array['status'], 400);
        $this->assertFalse($response_array['success']);
        $this->assertNull($response_array['data']);
    }

    /**
     * Test case to check for invalid access token
     */
    public function testGetMasterpassCheckoutsWithInvalidToken()
    {
        $this->unauthorizedResponseTest('GET', 'api/users/masterpass/checkouts');
    }
}